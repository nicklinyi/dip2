 2-D dip estimation by plane wave destruction
======
Copyright (C) 2004 University of Texas at Austin<br>
Copyright (C) 2016 Institute of Geology & Geophysics, Chinese Academy of Science
s.

Author: Yi Lin<br>
Email : linyi@mail.iggcas.ac.cn


Compile
------
```console
cd dip2
make clean
make
```

Use
-----
There is a `p.wok` file in the root directory.
Change the parameters in `p.wok` and type `./dip2.exe p.wok` or `make test`in the terminal will run the code.
