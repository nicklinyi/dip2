/* Triangle smoothing */
/*
  Copyright (C) 2004 University of Texas at Austin

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "triangle.h"

#include "_bool.h"
/*^*/

#include "malloc.h"
#include "blas.h"

#ifndef _triangle_h

typedef struct Triangle *triangle;
/* abstract data type */
/*^*/

#endif

struct Triangle {
    float *tmp, wt;
    int np, nb, nx;
    bool box;
};


static void fold2 (int o, int d, int nx, int nb, int np,
		   float *x, const float* tmp);
static void doubint2 (int nx, float *xx, bool der);
static void triple2 (int o, int d, int nx, int nb,
		     const float* x, float* tmp, bool box, float wt);


triangle triangle_init (int  nbox /* triangle length */,
			      int  ndat /* data length */,
                             bool box  /* if box instead of triangle */)
/*< initialize >*/
{
   triangle tr;

   tr = (triangle) alloc(1,sizeof(*tr));

   tr->nx = ndat;
   tr->nb = nbox;
   tr->box = box;
   tr->np = ndat + 2*nbox;

   if (box) {
	tr->wt = 1.0/(2*nbox-1);
   } else {
	tr->wt = 1.0/(nbox*nbox);
   }

   tr->tmp = alloc1float(tr->np);

   return tr;
}


static void fold2 (int o, int d, int nx, int nb, int np,
		   float *x, const float* tmp)
{
    int i, j;

    /* copy middle */
    for (i=0; i < nx; i++)
	x[o+i*d] = tmp[i+nb];

    /* reflections from the right side */
    for (j=nb+nx; j < np; j += nx) {
	for (i=0; i < nx && i < np-j; i++)
	    x[o+(nx-1-i)*d] += tmp[j+i];
	j += nx;
	for (i=0; i < nx && i < np-j; i++)
	    x[o+i*d] += tmp[j+i];
    }

    /* reflections from the left side */
    for (j=nb; j >= 0; j -= nx) {
	for (i=0; i < nx && i < j; i++)
	    x[o+i*d] += tmp[j-1-i];
	j -= nx;
	for (i=0; i < nx && i < j; i++)
	    x[o+(nx-1-i)*d] += tmp[j-1-i];
    }
}



static void doubint2 (int nx, float *xx, bool der)
{
    int i;
    float t;


    /* integrate forward */
    t=0.;
    for (i=0; i < nx; i++) {
	t += xx[i];
	xx[i] = t;
    }

    if (der) return;

    /* integrate backward */
    t = 0.;
    for (i=nx-1; i >= 0; i--) {
	t += xx[i];
	xx[i] = t;
    }
}



static void triple2 (int o, int d, int nx, int nb, const float* x, float* tmp, bool box, float wt)
{
    int i;

    for (i=0; i < nx + 2*nb; i++) {
	tmp[i] = 0;
    }

    if (box) {
	cblas_saxpy(nx,  +wt,x+o,d,tmp+1   ,1);
	cblas_saxpy(nx,  -wt,x+o,d,tmp+2*nb,1);
    } else {
	cblas_saxpy(nx,  -wt,x+o,d,tmp     ,1);
	cblas_saxpy(nx,2.*wt,x+o,d,tmp+nb  ,1);
	cblas_saxpy(nx,  -wt,x+o,d,tmp+2*nb,1);
    }
}

void smooth2 (triangle tr  /* smoothing object */,
		 int o, int d    /* trace sampling */,
		 bool der        /* if derivative */,
		 float *x        /* data (smoothed in place) */)
/*< apply adjoint triangle smoothing >*/
{
    triple2 (o,d,tr->nx,tr->nb,x,tr->tmp, tr->box, tr->wt);
    doubint2 (tr->np,tr->tmp,(bool) (tr->box || der));
    fold2 (o,d,tr->nx,tr->nb,tr->np,x,tr->tmp);
}

void  triangle_close(triangle tr)
/*< free allocated storage >*/
{
    free (tr->tmp);
    free (tr);
}


/* 	$Id$	 */
