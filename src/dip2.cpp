#include <iostream>
#include <fstream>
#include <float.h>
#include <math.h>
#include <string>
#include <ctime>
using namespace std;

#define MAX_DIM 9

#include "malloc.h"
#include "dip3.h"
#include "mask6.h"

int main(int argc, char* argv[])
{
  int n123, niter, order, nj1,nj2, i,j, liter, dim;
  int n1, n2;
  int n[MAX_DIM], rect[3], nr, ir;
  int hasmask,hasidip;
  int verb;
  float p0, *u, *p, pmin, pmax, eps;
  bool **mm;
  string in, out, mask, idip0;
	string pfile;

  ifstream infp;
  ifstream maskfp;
  ifstream idipfp;
  ofstream outfp;

  // read paras from file
	ifstream ifs_pfile;
	string line;

  pfile = argv[1];

  // printf("argv[0]=%s\n",argv[1] );

  // pfile="p.wok";
	ifs_pfile.open(pfile.c_str(), ios::in);
	if(!ifs_pfile){
		cout << "Cannot open paras work file." << endl;
		return 0;
	}
  getline(ifs_pfile,line);
  ifs_pfile >> in >> dim >> n[0] >> n[1];
  // std::cout << "in= "<<in<<" dim= "<<dim<<" n[0]=" <<n[0]<< " n[1]= "<<n[1] << std::endl;

  getline(ifs_pfile,line);
  getline(ifs_pfile,line);
  ifs_pfile >> out;
  // std::cout << "out= " << out << std::endl;

  getline(ifs_pfile,line);
  getline(ifs_pfile,line);
  ifs_pfile >> niter >> liter >> rect[0] >> rect[1];
  // std::cout << "niter= "<<niter<<" liter= "<<liter<<" rect[0]= " <<rect[0]<<" rect[1]= "<<rect[1] << std::endl;
  getline(ifs_pfile,line);
  getline(ifs_pfile,line);
  ifs_pfile >> p0 >> order >> nj1;
  // std::cout << "p0= "<<p0<<" order= "<<order<<" nj1= "<<nj1 << std::endl;

  getline(ifs_pfile,line);
  getline(ifs_pfile,line);
  ifs_pfile >> verb >> hasmask >> hasidip;
  // std::cout << "verb="<<verb << " hasmask= "<< hasmask <<" hasidip= "<<hasidip << std::endl;

  getline(ifs_pfile,line);
  getline(ifs_pfile,line);
  ifs_pfile >> pmin >> pmax >> eps;
  // std::cout << "pmin= "<<pmin<<" pmax= "<<pmax<<" eps= "<<eps << std::endl;

  getline(ifs_pfile,line);
  getline(ifs_pfile,line);
  ifs_pfile >> mask;
  // std::cout << "mask=" <<mask << std::endl;

  getline(ifs_pfile,line);
  getline(ifs_pfile,line);
  ifs_pfile >>  idip0;
  // std::cout << " idip0= "<<idip0 << std::endl;

  ifs_pfile.close();
	// end reading paras from file.

  // pmin = -FLT_MAX;
  // pmax = FLT_MAX;

  // dim = sf_filedims(in,n);
  // dim = 2
  if (dim < 2) n[1]=1;
  n123 = n[0]*n[1];
  nr = 1;
  for (j=2; j < dim; j++) {
	   nr *= n[j];
  }

  n[2]= 1;
  rect[2]=1;
  nj2=1;

  /* initialize dip estimation */
  dip3_init(n[0], n[1], n[2], rect, liter, eps, false);

  u = alloc1float(n123);
  p = alloc1float(n123);

  if (hasmask) {
	mm = boolalloc2(n123,2);
	// mask = sf_input("mask");

    } else {
	mm = (bool**) alloc(2,sizeof(bool*));
	mm[0] = mm[1] = NULL;

    }



      for (ir=0; ir < nr; ir++) {
	if (verb) printf("slice %d of %d;\n", ir+1, nr);
    	if (hasmask) {
	    // sf_floatread(u,n123,mask);
      maskfp.open(mask.c_str(), ios::in | ios::binary);
	if(!maskfp){
		cout << "ERROR:cannot open mask data. EXIT" << endl;
		return 0;
	}
	maskfp.read((char*)u, sizeof(float)*n123);
	maskfp.close();
	    mask32 (false, order, nj1, nj2, n[0], n[1], n[2], u, mm);
	}

	/* read data */
  infp.open(in.c_str(), ios::in | ios::binary);
	if(!infp){
		cout << "ERROR:cannot open input data. EXIT" << endl;
		return 0;
	}
	infp.read((char*)u, sizeof(float)*n123);
	infp.close();

	// sf_floatread(u,n123,in);
  /* initialize t-x dip */
	if (hasidip) {
	    // sf_floatread(p,n123,idip0);
      idipfp.open(idip0.c_str(), ios::in | ios::binary);
	if(!idipfp){
		cout << "ERROR:cannot open idip data. EXIT" << endl;
		return 0;
	}
	idipfp.read((char*)p, sizeof(float)*n123);
	idipfp.close();
	} else {
	    for(i=0; i < n123; i++) {
		p[i] = p0;
	    }
	}

  /* estimate t-x dip */
	dip3(false, 1, niter, order, nj1, u, p, mm[0], pmin, pmax);


  	/* write t-x dip */
	// sf_floatwrite(p,n123,out);
  outfp.open(out.c_str(), ios::out | ios::binary);
	if(!outfp){
		cout << "ERROR:cannot open output file. EXIT" << endl;
		return 0;
	}
	outfp.write((char*)p, sizeof(float)*n123);
  outfp.close();
}
    if (verb) printf("End of Program\n");


  return 0;
}
