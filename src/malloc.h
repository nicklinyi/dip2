#ifndef MALLOC_H
#define MALLOC_H

#include <iostream>
using namespace std;
#include <cstdlib>
#include <math.h>

//#include "fcomplex.h"
//#include "dcomplex.h"
void *alloc (size_t n    /* number of elements */,
			  size_t size /* size of one element */);
/* allocate and free multi-dimensional arrays */
void *alloc1 (size_t n1, size_t size);
void *realloc1 (void *v, size_t n1, size_t size);
void **alloc2 (size_t n1, size_t n2, size_t size);
void ***alloc3 (size_t n1, size_t n2, size_t n3, size_t size);
void ****alloc4 (size_t n1, size_t n2, size_t n3, size_t n4, size_t size);
void *****alloc5 (size_t n1, size_t n2, size_t n3, size_t n4, size_t n5, size_t size);
void ******alloc6 (size_t n1, size_t n2, size_t n3, size_t n4, size_t n5, size_t n6,
                   size_t size);

void free1 (void *p);
void free2 (void **p);
void free3 (void ***p);
void free4 (void ****p);
void free5 (void *****p);
void free6 (void ******p);
int *alloc1int (size_t n1);
int *realloc1int (int *v, size_t n1);
int **alloc2int (size_t n1, size_t n2);
int ***alloc3int (size_t n1, size_t n2, size_t n3);
float *alloc1float (size_t n1);
float *realloc1float (float *v, size_t n1);
float **alloc2float (size_t n1, size_t n2);
float ***alloc3float (size_t n1, size_t n2, size_t n3);

float ****alloc4float (size_t n1, size_t n2, size_t n3, size_t n4);
void free4float (float ****p);
float *****alloc5float (size_t n1, size_t n2, size_t n3, size_t n4, size_t n5);
void free5float (float *****p);
float ******alloc6float (size_t n1, size_t n2, size_t n3, size_t n4, size_t n5, size_t n6);
void free6float (float ******p);
int ****alloc4int (size_t n1, size_t n2, size_t n3, size_t n4);
void free4int (int ****p);
int *****alloc5int (size_t n1, size_t n2, size_t n3, size_t n4, size_t n5);
void free5int (int *****p);
unsigned short ******alloc6ushort(size_t n1,size_t n2,size_t n3,size_t n4,
                                  size_t n5, size_t n6);
unsigned char *****alloc5uchar(size_t n1,size_t n2,size_t n3,size_t n4,
                               size_t n5);
void free5uchar(unsigned char *****p);
unsigned short *****alloc5ushort(size_t n1,size_t n2,size_t n3,size_t n4,
                                 size_t n5);
void free5ushort(unsigned short *****p);
unsigned char ******alloc6uchar(size_t n1,size_t n2,size_t n3,size_t n4,
                                size_t n5, size_t n6);
void free6uchar(unsigned char ******p);
unsigned short ******alloc6ushort(size_t n1,size_t n2,size_t n3,size_t n4,
                                  size_t n5, size_t n6);
void free6ushort(unsigned short ******p);

double *alloc1double (size_t n1);
double *realloc1double (double *v, size_t n1);
double **alloc2double (size_t n1, size_t n2);
double ***alloc3double (size_t n1, size_t n2, size_t n3);
//fcomplex *alloc1complex (size_t n1);
//fcomplex *realloc1complex (fcomplex *v, size_t n1);
//fcomplex **alloc2complex (size_t n1, size_t n2);
//fcomplex ***alloc3complex (size_t n1, size_t n2, size_t n3);
//
//dcomplex *alloc1dcomplex (size_t n1);
//dcomplex *realloc1dcomplex (dcomplex *v, size_t n1);
//dcomplex **alloc2dcomplex (size_t n1, size_t n2);
//dcomplex ***alloc3dcomplex (size_t n1, size_t n2, size_t n3);
bool *boolalloc (size_t n /* number of elements */);
/*@out@*/ bool **boolalloc2 (size_t n1 /* fast dimension */,
				size_t n2 /* slow dimension */);
void free1int (int *p);
void free2int (int **p);
void free3int (int ***p);
void free1float (float *p);
void free2float (float **p);
void free3float (float ***p);

void free1double (double *p);
void free2double (double **p);
void free3double (double ***p);
//void free1complex (fcomplex *p);
//void free2complex (fcomplex **p);
//void free3complex (fcomplex ***p);
//
//void free1dcomplex (dcomplex *p);
//void free2dcomplex (dcomplex **p);
//void free3dcomplex (dcomplex ***p);

void s_init_array(int *p, int n1);
void s_init_array(int **p, int n1, int n2);
void s_init_array(int ***p, int n1, int n2, int n3);

void s_init_array(float *p, int n1);
void s_init_array(float **p, int n1, int n2);
void s_init_array(float ***p, int n1, int n2, int n3);

void s_init_array(double *p, int n1);
void s_init_array(double **p, int n1, int n2);
void s_init_array(double ***p, int n1, int n2, int n3);

//void s_init_array(fcomplex *p, int n1);
//void s_init_array(fcomplex **p, int n1, int n2);
//void s_init_array(fcomplex ***p, int n1, int n2, int n3);
//
//void s_init_array(dcomplex *p, int n1);
//void s_init_array(dcomplex **p, int n1, int n2);
//void s_init_array(dcomplex ***p, int n1, int n2, int n3);




#endif
