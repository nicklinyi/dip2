# Makefile for dip2

CPP = g++
EXE = ./bin/dip2.exe

.PHONY: dip2

dip2: 
	cd src;make

install:
	mkdir bin; cp src/*.exe bin/

test: $(EXE) p.wok
	$(EXE) p.wok

clean:
	cd src; make clean; 
